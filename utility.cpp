//
// Created by andrew on 12.03.18.
//

#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QtGui/QColor>
#include "qcustomplot.h"

void
plot(std::vector<double> &x, std::vector<double> &y, QCustomPlot *plot, QColor color, int plotNum, const std::string &plotName)
{
    QVector<double> X = QVector<double>::fromStdVector(x);
    QVector<double> Y = QVector<double>::fromStdVector(y);

    plot->resize(512, 512);
    plot->legend->setVisible(true);
    plot->legend->setFont(QFont("Helvetica", 9));
    plot->addGraph();
    plot->graph(plotNum)->setName(QString::fromStdString(plotName));
    plot->graph(plotNum)->setPen(QPen(color));
    plot->graph(plotNum)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 2));
    plot->graph(plotNum)->setData(X, Y);

    plot->graph(plotNum)->rescaleAxes();
    plot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

    plot->show();
}

double calculateEntropy(const cv::Mat &in)
{
    std::vector<double> probabilites(256);

    for (int i = 0; i < in.rows; i++)
    {
        for (int j = 0; j < in.cols; j++)
        {
            //uchar diff = in.at<uchar>(i, j + 1) - in.at<uchar>(i, j);
            probabilites[in.at<uchar>(i, j)]++;
        }
    }

    double entropy = 0;
    for (double &probability : probabilites)
    {
        probability /= in.rows * in.cols;

        if (probability != 0)
        {
            entropy -= probability * std::log(probability);
        }
    }

    entropy /= std::log(2);
    probabilites.clear();
    return entropy;
}

void showHistogram(cv::Mat &img, const std::string &name)
{
    int histSize = 512;
    float range[] = { 0, 256 };
    const float *histRange = { range };
    bool uniform = true;
    bool accumulate = false;
    int histWidth = 512;
    int histHeight = 400;
    int binWidth = cvRound((double) histWidth / histHeight);

    cv::Mat hist;
    cv::calcHist(&img, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange, uniform, accumulate);
    cv::Mat histImgDark(histHeight, histWidth, CV_8UC3, cv::Scalar(0, 0, 0));
    cv::normalize(hist, hist, 0, histImgDark.rows, cv::NORM_MINMAX, -1, cv::Mat());
    for (int i = 1; i < histSize; i++)
    {
        line(histImgDark, cv::Point(binWidth * (i - 1), histHeight - cvRound(hist.at<float>(i - 1))),
             cv::Point(binWidth * (i), histHeight - cvRound(hist.at<float>(i))),
             cv::Scalar(255, 255, 255), 2, 8, 0);
    }
    cv::imshow(name, histImgDark);
}
