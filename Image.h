#ifndef BMP_IMAGE_H
#define BMP_IMAGE_H

#include <cstdint>
#include "qcustomplot.h"

#pragma pack(push, 1)
typedef struct                       /**** BMP file header structure ****/
{
    uint16_t    bfType;           /* Magic number for file */
    uint32_t    bfSize;           /* Size of file */
    uint16_t    bfReserved1;      /* Reserved */
    uint16_t    bfReserved2;      /* ... */
    uint32_t    bfOffBits;        /* Offset to bitmap data */
} FILE_HDR;
#pragma(pop)

#pragma pack(push, 1)
typedef struct                       /**** BMP file info structure ****/
{
    uint32_t    biSize;           /* Size of info header */
    int32_t     biWidth;          /* Width of image */
    int32_t     biHeight;         /* Height of image */
    uint16_t    biPlanes;         /* Number of color planes */
    uint16_t    biBitCount;       /* Number of bits per pixel */
    uint32_t    biCompression;    /* Type of compression to use */
    uint32_t    biSizeImage;      /* Size of image data */
    int32_t     biXPelsPerMeter;  /* X pixels per meter */
    int32_t     biYPelsPerMeter;  /* Y pixels per meter */
    uint32_t    biClrUsed;        /* Number of colors used */
    uint32_t    biClrImportant;   /* Number of important colors */
} INFO_HDR;
#pragma(pop)

typedef struct
{
    uint8_t b;
    uint8_t g;
    uint8_t r;
} ColorRGB;


class Image
{
public:
    Image() = default;

    Image(const std::string &filename);
    Image(const Image &image);
    ~Image();

    void writeImageTo(const std::string &filename);
    void writeComponentsTo(const std::string &filename);
    void calculateCorrelation();
    void calculateCorrelation(const Image& other, std::vector<std::vector<double>> &chan);
    void autocorrelation(QCustomPlot *redPlot, QCustomPlot *greenPlot, QCustomPlot *bluePlot);
    void calculatePSNR(const Image &src, bool first = true, bool second = true, bool third = true);
    void decimationAtN(Image &rgbSrc, Image &ycbcrSrc, int times);

    Image convertToYCbCr();
    Image convertToRGB();

    ColorRGB *getData() { return img;    }
    int32_t getWidth()  { return width; }
    int32_t getHeight() { return height; }
    Image cut(int x, int y, bool left = false, bool top = false);
    const FILE_HDR *getFileHeader() { return fileHeader; }
    const INFO_HDR *getInfoHeader() { return infoHeader; }

private:
    int32_t     width;
    int32_t     height;
    uint32_t    infoSize;
    FILE_HDR    *fileHeader;
    INFO_HDR    *infoHeader;
    ColorRGB    *img;
    std::string type;
};


#endif
