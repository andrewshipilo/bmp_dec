#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <vector>
#include "Image.h"
#include "qcustomplot.h"
#include "utility.h"


Image::Image(const std::string &filename)
{
    type = "RGB";
    FILE *file;
    ColorRGB *data = nullptr;

    if (!(file = fopen(filename.c_str(), "rb")))
    {
        printf("Error occured while opening file\n");
        return;
    }

    // Load File Header data
    fileHeader = (FILE_HDR *) malloc(sizeof(FILE_HDR));

    if (!fread((void *) fileHeader, 14, 1, file))
    {
        printf("Failed to read BMP header data\n");
        return;
    }

    // Load Info Header data
    fread(&infoSize, sizeof(uint32_t), 1, file);

    fseek(file, -sizeof(uint32_t), SEEK_CUR);
    infoHeader = (INFO_HDR *) malloc(infoSize);

    if (!fread((void *) infoHeader, infoSize, 1, file))
    {
        printf("Failed to read DIB header data\n");
        return;
    }

    if (infoSize < 2097152)
    {
        fseek(file, fileHeader->bfOffBits, SEEK_SET);

        data = (ColorRGB *) malloc(infoHeader->biSizeImage);

        if (!fread((void *) data, sizeof(ColorRGB), infoHeader->biSizeImage / sizeof(ColorRGB), file))
        {
            printf("Failed to read BMP pixel data\n");
            return;
        }
    }

    img = data;
    width = infoHeader->biWidth;
    height = infoHeader->biHeight;
    fclose(file);
}

Image::~Image()
{
    delete infoHeader;
    delete fileHeader;
    delete img;
}

Image::Image(const Image &image)
{
    fileHeader = new FILE_HDR(*image.fileHeader);
    infoHeader = new INFO_HDR(*image.infoHeader);
    img = (ColorRGB *) malloc(infoHeader->biSizeImage);

    for (int i = 0; i < image.width * image.height; i++)
    {
        img[i].r = image.img[i].r;
        img[i].g = image.img[i].g;
        img[i].b = image.img[i].b;
    }

    width = image.width;
    height = image.height;
    infoSize = image.infoSize;
    type = image.type;
}

void Image::writeImageTo(const std::string &filename)
{
    FILE *file = fopen(filename.c_str(), "wb");
    fwrite((void *) fileHeader, 14, 1, file);
    fwrite((void *) infoHeader, 40, 1, file);
    fwrite((void *) img, sizeof(ColorRGB),
           infoHeader->biWidth * infoHeader->biHeight, file);
    fclose(file);
}


void Image::calculateCorrelation()
{
    auto dataSize = width * height;
    double evR = 0, evG = 0, evB = 0; // Expected value

    for (int i = 0; i < dataSize; i++)
    {
        evR += img[i].r;
        evG += img[i].g;
        evB += img[i].b;
    }

    evR /= dataSize;
    evG /= dataSize;
    evB /= dataSize;

    double evRG = 0, evRB = 0, evBG = 0;
    double sdR = 0, sdG = 0, sdB = 0; // Standard deviation

    for (int i = 0; i < dataSize; i++)
    {
        double R = img[i].r - evR;
        double G = img[i].g - evG;
        double B = img[i].b - evB;

        evRG += R * G;
        evRB += R * B;
        evBG += B * G;

        sdR += R * R;
        sdG += G * G;
        sdB += B * B;
    }

    evRG /= dataSize;
    evRB /= dataSize;
    evBG /= dataSize;

    sdR = std::sqrt(sdR / (width * (height - 1)));
    sdG = std::sqrt(sdG / (width * (height - 1)));
    sdB = std::sqrt(sdB / (width * (height - 1)));

    // Calculate correlation coeff

    double corrRG = evRG / (sdR * sdG);
    double corrRB = evRB / (sdR * sdB);
    double corrBG = evBG / (sdB * sdG);

    std::cout << "Correlation between " << type[0] << " and " << type[1] << " " << corrRG << std::endl;
    std::cout << "Correlation between " << type[0] << " and " << type[2] << " " << corrRB << std::endl;
    std::cout << "Correlation between " << type[1] << " and " << type[2] << " " << corrBG << "\n\n";
}


void Image::calculateCorrelation(const Image &other, std::vector<std::vector<double>> &chan)
{
    auto dataSize = width * height;
    double ev1R = 0, ev1G = 0, ev1B = 0;
    double ev2R = 0, ev2G = 0, ev2B = 0; // Expected value

    for (int i = 0; i < dataSize; i++)
    {
        ev1R += img[i].r;
        ev1G += img[i].g;
        ev1B += img[i].b;

        ev2R += other.img[i].r;
        ev2G += other.img[i].g;
        ev2B += other.img[i].b;
    }

    ev1R /= dataSize, ev1G /= dataSize, ev1B /= dataSize;
    ev2R /= dataSize, ev2G /= dataSize, ev2B /= dataSize;

    double evRR = 0, evGG = 0, evBB = 0;
    double sd1R = 0, sd1G = 0, sd1B = 0;
    double sd2R = 0, sd2G = 0, sd2B = 0; // Standard deviation

    for (int i = 0; i < dataSize; i++)
    {
        double R1 = img[i].r - ev1R;
        double G1 = img[i].g - ev1G;
        double B1 = img[i].b - ev1B;

        double R2 = other.img[i].r - ev2R;
        double G2 = other.img[i].g - ev2G;
        double B2 = other.img[i].b - ev2B;
        
        evRR += R1 * R2;
        evGG += G1 * G2;
        evBB += B1 * B2;

        sd1R += R1 * R1;
        sd1G += G1 * G1;
        sd1B += B1 * B1;

        sd2R += R2 * R2;
        sd2G += G2 * G2;
        sd2B += B2 * B2;
    }

    evRR /= dataSize;
    evGG /= dataSize;
    evBB /= dataSize;

    sd1R = std::sqrt(sd1R / (width * (height - 1)));
    sd1G = std::sqrt(sd1G / (width * (height - 1)));
    sd1B = std::sqrt(sd1B / (width * (height - 1)));

    sd2R = std::sqrt(sd2R / (width * (height - 1)));
    sd2G = std::sqrt(sd2G / (width * (height - 1)));
    sd2B = std::sqrt(sd2B / (width * (height - 1)));

    // Calculate correlation coeff
    double corrRR = evRR / (sd1R * sd2R);
    double corrGG = evGG / (sd1G * sd2G);
    double corrBB = evBB / (sd1B * sd2B);

    chan[0].push_back(corrRR);
    chan[1].push_back(corrGG);
    chan[2].push_back(corrBB);

    /*std::cout << "Correlation between R1 and R2 " << corrRR << std::endl;
    std::cout << "Correlation between G1 and G2 " << corrGG << std::endl;
    std::cout << "Correlation between B1 and B2 " << corrBB << "\n\n";*/
}

uint8_t clip(double a)
{
    if (a < 0)
    {
        return 0;
    } else if (a > 255)
    {
        return 255;
    } else
    {
        return (uint8_t) a;
    }
}

Image Image::convertToYCbCr()
{
    Image image(*this);

    for (int i = 0; i < width * height; i++)
    {
        image.img[i].r = clip(0.299 * img[i].r + 0.587 * img[i].g + 0.114 * img[i].b);
        image.img[i].g = clip(0.5643 * (img[i].b - image.img[i].r) + 128);
        image.img[i].b = clip(0.7132 * (img[i].r - image.img[i].r) + 128);
    }
    image.type = "YUV";
    return image;
}

Image Image::convertToRGB()
{
    Image image(*this);

    for (int i = 0; i < width * height; i++)
    {
        image.img[i].g = clip(img[i].r - 0.714 * (img[i].b - 128) - 0.334 * (img[i].g - 128));
        image.img[i].r = clip(img[i].r + 1.402 * (img[i].b - 128));
        image.img[i].b = clip(img[i].r + 1.772 * (img[i].g - 128));
    }
    image.type = "RGB";
    return image;
}

void Image::writeComponentsTo(const std::string &filename)
{
    Image lenaR(*this);
    Image lenaG(*this);
    Image lenaB(*this);

    auto lenaRData = lenaR.getData();
    auto lenaGData = lenaG.getData();
    auto lenaBData = lenaB.getData();
    bool YCbCr = false;

    if (type[0] == 'Y') { YCbCr = true; }

    for (int i = 0; i < width * height; i++)
    {
        lenaRData[i].r = img[i].r;
        lenaRData[i].g = YCbCr ? img[i].r : 0;
        lenaRData[i].b = YCbCr ? img[i].r : 0;

        lenaGData[i].r = YCbCr ? img[i].g : 0;
        lenaGData[i].g = img[i].g;
        lenaGData[i].b = YCbCr ? img[i].b : 0;

        lenaBData[i].r = YCbCr ? img[i].b : 0;
        lenaBData[i].g = YCbCr ? img[i].g : 0;
        lenaBData[i].b = YCbCr ? 0 : img[i].b;
    }

    lenaR.writeImageTo(filename + type[0] + ".bmp");
    lenaG.writeImageTo(filename + type[1] + ".bmp");
    lenaB.writeImageTo(filename + type[2] + ".bmp");
}

void Image::calculatePSNR(const Image &src, bool first /*= true*/, bool second /*= true*/, bool third /*= true*/)
{
    double psnrR = 0, psnrG = 0, psnrB = 0;
    for (int i = 0; i < width * height; i++)
    {
        psnrR += std::pow(src.img[i].r - img[i].r, 2);
        psnrG += std::pow(src.img[i].g - img[i].g, 2);
        psnrB += std::pow(src.img[i].b - img[i].b, 2);
    }

    psnrR = 10 * std::log10((width * height * std::pow((std::pow(2, 8) - 1), 2)) / psnrR);
    psnrG = 10 * std::log10((width * height * std::pow((std::pow(2, 8) - 1), 2)) / psnrG);
    psnrB = 10 * std::log10((width * height * std::pow((std::pow(2, 8) - 1), 2)) / psnrB);
    if (first)
        std::cout << "PSNR 1 " << type[0] << "   : " << psnrR << std::endl;
    if (second)
        std::cout << "PSNR 2 " << type[1] << "   : " << psnrG << std::endl;
    if (third)
        std::cout << "PSNR 3 " << type[2] << "   : " << psnrB << "\n\n";
}

void Image::decimationAtN(Image &rgbSrc, Image &ycbcrSrc, int times)
{
    auto dataSize = rgbSrc.getWidth() * rgbSrc.getHeight();

    Image temp1(ycbcrSrc);
    Image temp2(ycbcrSrc);
    auto temp1Data = temp1.getData();
    auto temp2Data = temp2.getData();

    std::vector<ColorRGB> decimation2A;
    std::vector<ColorRGB> decimation2B;

    /* Method A */
    for (int i = 0; i < dataSize; i += rgbSrc.getWidth() * times)
    {
        for (int j = 0; j < rgbSrc.getWidth(); j += times)
        {
            decimation2A.push_back(temp1Data[i + j]);
        }
    }

    std::cout << "Data size: " << dataSize << std::endl;
    std::cout << "Decimation" << times << "A size: " << decimation2A.size() << std::endl;

    /* Method B */
    for (int i = 0, x = -1; i < dataSize - width; i += times)
    {
        if (i % width == 0)
        {
            x++;
        }
        if (x % 2 == 1)
        {
            if (i == dataSize - 3 * width)
            {
                break;
            }
            i += (times - 1) * width;
            x++;
        }

        ColorRGB newItem = {};
        double sumG = 0, sumB = 0;
        for (int j = 0; j < times; j++)
        {
            for (int k = 0; k < times; k++)
            {
                sumG += temp2Data[i + j + width * k].g;
                sumB += temp2Data[i + j + width * k].b;
            }
        }
        newItem.g = static_cast<uint8_t>(sumG / (times * times));
        newItem.b = static_cast<uint8_t>(sumB / (times * times));
        decimation2B.push_back(newItem);
    }

    std::cout << "Decimation"<< times << "B size: " << decimation2B.size() << std::endl;

    /* Task 9 */

    Image res2A(ycbcrSrc);
    auto res2AData = res2A.getData();

    Image res2B(ycbcrSrc);
    auto res2BData = res2B.getData();

    for (int i = 0, x = -1, z = 0; z < decimation2B.size(); i += times, z++)
    {
        if (i % rgbSrc.getWidth() == 0 && i != 0)
        {
            x++;
        }

        if (x % 2 == 1)
        {
            i += (times - 1) * rgbSrc.getWidth();
            x++;
        }

        for (int j = 0; j < times; j++)
        {
            for (int k = 0; k < times; k++)
            {
                res2AData[i + j + width * k].g = decimation2A[z].g;
                res2AData[i + j + width * k].b = decimation2A[z].b;

                res2BData[i + j + width * k].g = decimation2B[z].g;
                res2BData[i + j + width * k].b = decimation2B[z].b;
            }
        }
    }

    Image RGBRes2A(res2A.convertToRGB());
    std::string res = "res";
    RGBRes2A.writeImageTo(res + std::to_string(times) + "A.bmp");

    Image RGBRes2B(res2B.convertToRGB());
    RGBRes2B.writeImageTo(res + std::to_string(times) + "B.bmp");

    /* Task 10 */

    std::cout << "\nPSNR, A method, " << times << " times:\n";
    res2A.calculatePSNR(ycbcrSrc, false);
    RGBRes2A.calculatePSNR(rgbSrc);

    std::cout << "\nPSNR, B method, " << times << " times:\n";
    res2B.calculatePSNR(ycbcrSrc, false);
    RGBRes2B.calculatePSNR(rgbSrc);
}

Image Image::cut(int x, int y, bool left /*=false*/, bool top /*= false*/)
{
    auto fromY = top ? y : 0;
    auto fromX = left ? x : 0;
    auto toY = top ? height : height - y;
    auto toX = left ? width : width - x;

    Image res;
    size_t imgSize = (width - x) * (height - y) * 3;
    res.fileHeader = new FILE_HDR(*this->fileHeader);
    res.infoHeader = new INFO_HDR(*this->infoHeader);
    res.img = (ColorRGB *) malloc(imgSize);

    for (int i = fromY; i < toY; i++)
    {
        for (int j = fromX; j < toX; j++)
        {
            auto cell = (i - fromY) * (toX - fromX) + (j - fromX);
            auto cell2 = i * width + j;
            res.img[cell].r = img[cell2].r;
            res.img[cell].g = img[cell2].g;
            res.img[cell].b = img[cell2].b;
        }
    }

    res.fileHeader->bfSize = imgSize + 54;
    res.infoHeader->biSizeImage = static_cast<uint32_t>(imgSize);
    res.infoHeader->biWidth = (width - x);
    res.infoHeader->biHeight = (height - y);

    res.width = (width - x);
    res.height = (height - y);
    res.infoSize = infoSize;
    res.type = type;

    return res;
}

void Image::autocorrelation(QCustomPlot *redPlot, QCustomPlot *greenPlot, QCustomPlot *bluePlot)
{
    bool top = false;
    auto Y = {0, 5, -5, 10, -10};
    auto plotNum = 0;

    QString name1;
    name1.append(type[0]).append(" autocorr");
    redPlot->setWindowTitle(name1);

    QString name2;
    name2.append(type[1]).append(" autocorr");
    greenPlot->setWindowTitle(name2);

    QString name3;
    name3.append(type[2]).append(" autocorr");
    bluePlot->setWindowTitle(name3);

    for (auto y : Y)
    {
        std::vector<std::vector<double>> channels(3);
        std::vector<double> axes;
        std::string name = "y = " + std::to_string(y);
        if (y < 0)
        {
            y *= -1;
            top = true;
        }

        for (int x = 0; x < width / 4; x += 4)
        {
            axes.push_back((double) x);
            Image first = cut(x, y, false, top);
            Image second = cut(x, y, true, top);

            first.calculateCorrelation(second, channels);
        }

        plot(axes, channels[0], redPlot, Qt::GlobalColor(Qt::red + plotNum) , plotNum, name);
        plot(axes, channels[1], greenPlot, Qt::GlobalColor(Qt::red + plotNum), plotNum, name);
        plot(axes, channels[2], bluePlot, Qt::GlobalColor(Qt::red + plotNum), plotNum, name);
        plotNum++;
    }


}


