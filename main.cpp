#include <cmath>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QtCore/QVector>
#include <QtGui/QColor>
#include "Image.h"
#include "qcustomplot.h"
#include "utility.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    auto plotR = new QCustomPlot();
    auto plotG = new QCustomPlot();
    auto plotB = new QCustomPlot();

    auto plotY = new QCustomPlot();
    auto plotU = new QCustomPlot();
    auto plotV = new QCustomPlot();


    Image lena("lena.bmp");
    auto lenaData = lena.getData();
    auto dataSize = lena.getWidth() * lena.getHeight();


    /* Task 3 */
    lena.writeComponentsTo("lena");

    /*Image shiftedLena = lena.cut(64, 0, true);
    lena.calculateCorrelation(shiftedLena);
    shiftedLena.writeImageTo("shiftedLena.bmp");*/

    /* Task 4 */
    lena.calculateCorrelation();
    lena.autocorrelation(plotR, plotG, plotB);

    /* Task 5 */
    Image lenaYCbCr(lena.convertToYCbCr());
    lenaYCbCr.calculateCorrelation();
    lenaYCbCr.writeImageTo("lenaYCbCr.bmp");
    lenaYCbCr.autocorrelation(plotY, plotU, plotV);

    /* Task 6 */
    lenaYCbCr.writeComponentsTo("lena");

    /* Task 7 */
    Image YCbCrToRGB(lenaYCbCr.convertToRGB());
    YCbCrToRGB.writeComponentsTo("lenaRGBFromYCbCr");
    YCbCrToRGB.writeImageTo("lenaRGBFromYCbCr.bmp");
    YCbCrToRGB.calculatePSNR(lena);

    /* Task 8-10 */
    lenaYCbCr.decimationAtN(lena, lenaYCbCr, 2);

    /* Task 11 */
    lenaYCbCr.decimationAtN(lena, lenaYCbCr, 4);

    /* Task 12 */
    cv::Mat lenaRGB = cv::imread("lena.bmp", 1);
    cv::Mat lenaYUV = cv::imread("lenaYCbCr.bmp", 1);

    cv::imshow("RGB", lenaRGB);
    cv::imshow("YUV", lenaYUV);

    std::vector<cv::Mat> channelsRGB;
    cv::split(lenaRGB, channelsRGB);
    cv::Mat R = channelsRGB[0];
    cv::Mat G = channelsRGB[1];
    cv::Mat B = channelsRGB[2];

    showHistogram(R, "RED ch");
    showHistogram(G, "GREEN ch");
    showHistogram(B, "BLUE ch");

    std::vector<cv::Mat> channelsYUV;
    cv::split(lenaYUV, channelsYUV);
    cv::Mat Y = channelsYUV[2];
    cv::Mat U = channelsYUV[1];
    cv::Mat V = channelsYUV[0];

    showHistogram(Y, "Y ch");
    showHistogram(U, "U ch");
    showHistogram(V, "V ch");

    /* Task 13 */
    std::cout << "R entropy:    " << calculateEntropy(R) << std::endl;
    std::cout << "G entropy:    " << calculateEntropy(G) << std::endl;
    std::cout << "B entropy:    " << calculateEntropy(B) << std::endl << std::endl;

    std::cout << "Y entropy:    " << calculateEntropy(Y) << std::endl;
    std::cout << "U entropy:    " << calculateEntropy(U) << std::endl;
    std::cout << "V entropy:    " << calculateEntropy(V) << std::endl << std::endl;

    cv::waitKey(0);

    delete plotR;
    delete plotG;
    delete plotB;
    delete plotY;
    delete plotU;
    delete plotV;
    return 0;
}