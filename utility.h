#ifndef BMP_UTILITY_H
#define BMP_UTILITY_H

#include <cmath>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <QtCore/QVector>
#include <QtGui/QColor>
#include "qcustomplot.h"

extern void plot(std::vector<double> &x, std::vector<double> &y, QCustomPlot *plot,
                 QColor color, int plotNum, const std::string &plotName);

extern double calculateEntropy(const cv::Mat &in);

extern void showHistogram(cv::Mat &img, const std::string &name);

#endif
